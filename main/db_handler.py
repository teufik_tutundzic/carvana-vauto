from main.models import db, ResultModel


def format_field_names(collection, db_names, view_names):
    for new_key, old_key in zip(db_names, view_names):
        collection[new_key] = collection.pop(old_key)


def define_result_instances(input_values):
    if not ResultModel.query.filter_by(url=input_values['url']).first():
        return ResultModel(**input_values)


def save_result_instances(result_instances):
    db.session.bulk_save_objects(result_instances)
    db.session.commit()


def update_after_processing(vin_status_list):
    for element in vin_status_list:
        try:
            vin_data = ResultModel.query.filter_by(url=element['url']).first()
            if vin_data:

                desired_values = element.get('desired_values')
                if desired_values:
                    save_desired_values(vin_data, desired_values)

                checked_options = element.get('formatted_checked_options')
                if checked_options:
                    save_desired_options(vin_data, checked_options)
        except Exception as e:
            error_message = "Processing of VIN:{} resulted in:{}".format(element['vin'], e.message or e.msg)
            # log.error(error_message)
    db.session.commit()


def save_desired_values(vin_data, desired_values):
    desired_values['odometer_adj'] = desired_values['eAverage'] - desired_values.pop('average') - \
                                     desired_values.pop('o')
    db_names = ['num_vehicles', 'avg_market_price', 'avg_odometer', 'year', 'make', 'model', 'odometer_adj']
    view_names = ['racvc', 'eAverage', 'averageo', 'ModelYear', 'Make', 'Model', 'odometer_adj']
    format_field_names(desired_values, db_names, view_names)
    [setattr(vin_data, attr, value) for attr, value in desired_values.items()]


def format_checked_option_names(view_name):
    mapper = {'Series': 'series', 'SeriesDetail': 'series_detail', 'EngineCylinderCount': 'num_cylinders',
              'DriveTrainType': 'drive_train_type', 'TransmissionType': 'transmission',
              'EngineDisplacement': 'displacement', 'BodyCabStyle': 'cab_style'}
    return mapper.get(view_name)


def save_desired_options(vin_data, checked_options):
    for option in checked_options:
        db_option_name = format_checked_option_names(option['name'])
        if db_option_name:
            setattr(vin_data, db_option_name, option['value'])
