from main.db_handler import update_after_processing

from main.parser import *
from main.config import Config

from main.app import celery


@celery.task()
def execute_scraping_and_forward_results(cookie, vin_url):
    log = execute_scraping_and_forward_results.get_logger()
    try:
        headers = {'Cookie': cookie}
        login_response_html = fetch_site(vin_url['url'], headers)

        form_data = construct_radar_data_request(login_response_html)
        if form_data['Make'] and form_data['Model']:
            headers['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8'
            radar_response_html = fetch_site(Config.RADAR_URL, headers, data=form_data)

            checked_options = find_checked_options(radar_response_html)
            vin_url['formatted_checked_options'] = format_checked_options(checked_options)

            final_form_data = construct_a_field(checked_options, form_data)
            final_response_html = fetch_site(Config.RADAR_URL, headers, data=final_form_data)
            desired_values = extract_desired_values(final_response_html)
            add_basic_fields(desired_values, form_data)
            vin_url['desired_values'] = desired_values
            log.info("Finished with {}".format(str(vin_url)))
    except Exception as e:
        error_message = "Processing of VIN:{} resulted in:{}".format(vin_url['vin'], str(e))
        vin_url['error'] = error_message
        log.error(error_message)
    return vin_url


@celery.task()
def save_and_log_results(vin_status_list):
    log = save_and_log_results.get_logger()
    try:
        update_after_processing(vin_status_list)
        message = construct_notification_message(vin_status_list)
        log.info(message)
    except Exception as e:
        log.error(str(e))
