import pytest

from main.app import create_app


@pytest.fixture(scope="session")
def app():
    """Returns session-wide application.
    """
    app = create_app('dev')
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    app.config['TESTING'] = True
    return app


@pytest.fixture
def client(app):
    return app.test_client()


# @pytest.mark.parametrize('vins_provider_ids', [
#     [['2G1WF5E36D1194982', 16]
#      ]
# ])
# def test_process_urls(client, vins_provider_ids):
#     test_data = {"vins-pids": vins_provider_ids}
#     response = client.post('/api/vauto/process_urls', json=test_data)
#     assert response.status == 200

def test_data_from_csv(client):
    import csv

    with open('/Users/teufiktutundzic/Desktop/carvana_cred/vehicle-2018-10-26-VAUTO-INPUT-DATA.csv') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        vins_pids = [[row[0], row[8]] for row in csv_reader]
        test_data = {"vins-pids": vins_pids[15000:25000]}
        response = client.post('/api/vauto/process_urls', json=test_data)
        assert response.status_code == 200