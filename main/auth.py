from selenium import webdriver
from selenium.webdriver.chrome.options import Options

from werkzeug.contrib.cache import SimpleCache

from main import config

cache = SimpleCache()


def webdriver_auth():
    try:
        chrome_options = Options()
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("--window-size=1920x1080")

        driver = webdriver.Chrome(chrome_options=chrome_options, executable_path=config.CHROME_DRIVER)

        driver.get(config.LOGIN_URL)
        username = driver.find_element_by_id('X_PageBody_ctl00_ctl00_Login1_UserName')
        password = driver.find_element_by_id('X_PageBody_ctl00_ctl00_Login1_Password')

        username.send_keys(config.LOGIN_USERNAME)
        password.send_keys(config.LOGIN_PASSWORD)

        driver.find_element_by_name("X$PageBody$ctl00$ctl00$Login1$LoginButton").click()

        cookie_str = '_locale=en-US; ASP.NET_SessionId={}; _ce=1; usedAutoLogin=false; ' \
                     'CurrentEntity=DJ2ar2-ACTmCd603Zqfml3A_K5NchwPmk14NR6k9Xt0=; _theme=Provision; _' \
                     'unauththeme=Provision; _ga=GA1.1.1892972077.1535383794; _gid=GA1.1.1110050934.1535383796; __' \
                     'utmt=1; __utma=6442421.1892972077.1535383794.1535460100.1535460100.1; __utmc=6442421; __' \
                     'utmz=6442421.1535460100.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); __u' \
                     'tmb=6442421.1.10.1535460100; isCaBridgeEligible=True; isCaBridgeUser=False; vAutoAuth={}'

        session_id = driver.get_cookie('ASP.NET_SessionId')['value']
        v_auto_auth = driver.get_cookie('vAutoAuth')['value']

        return cookie_str.format(session_id, v_auto_auth)
    except Exception as e:
        return False


def handle_cookie():
    cookie = cache.get('Cookie')
    if not cookie:
        cookie = webdriver_auth()
        if not cookie:
            return
    cache.set('Cookie', cookie, timeout=config.CACHE_TIMEOUT)
    return cookie
