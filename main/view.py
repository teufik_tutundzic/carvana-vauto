from flask import request, Blueprint
from main.auth import handle_cookie
from main.tasks import *

from celery import chord
from werkzeug.contrib.cache import SimpleCache

cache = SimpleCache()
blueprint = Blueprint('api', __name__, url_prefix='/api/vauto')


@blueprint.route('/process_urls', methods=["POST"])
def process_urls():
    if request.method == "POST":
        vins_provider_ids = request.json.get('vins-pids')

        add_vauto_urls(vins_provider_ids)
        v_auto_data = save_and_format_vin_data(vins_provider_ids)

        cookie = handle_cookie()
        if not cookie:
            return json.dumps({'success': False, 'reason': 'Invalid Credentials'}), 401

        chord((execute_scraping_and_forward_results.s(cookie, vin_url) for vin_url in v_auto_data),
              save_and_log_results.s())()
        return json.dumps({'success': True}), 200
