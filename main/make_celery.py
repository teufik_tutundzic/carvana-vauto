def init_celery(app, celery):
    app.config['CELERY_TRACK_STARTED'] = True
    app.config['CELERY_SEND_EVENTS'] = True
    app.config['CELERY_RESULT_BACKEND'] = app.config.get('CELERY_BACKEND_URL')
    # app.config['CELERYD_TASK_TIME_LIMIT'] = 25
    app.config['CELERYD_PREFETCH_MULTIPLIER'] = 1

    # create context tasks in celery
    celery.conf.update(app.config)
    TaskBase = celery.Task

    class ContextTask(TaskBase):
        abstract = True

        def __call__(self, *args, **kwargs):
            with app.app_context():
                return TaskBase.__call__(self, *args, **kwargs)

    celery.Task = ContextTask
