import os
from flask import Flask
from .config import config_by_name
from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager
import logging
from logging.handlers import RotatingFileHandler

from main import celery
from main.make_celery import init_celery
import main.view as view
from main.models import db, ResultModel


def create_app(config_name):
    app = Flask(__name__)
    app.config.from_object(config_by_name[config_name])
    init_celery(app, celery=celery)
    register_blueprints(app)
    configure_extensions(app)
    return app


def register_blueprints(app):
    app.register_blueprint(view.blueprint)


def configure_extensions(app):
    db.app = app
    db.init_app(app)


flask_app = create_app(os.getenv('CARVANA_ENV') or 'dev')
flask_app.app_context().push()

migrate = Migrate(flask_app, db)
migrate.init_app(flask_app, db)

manager = Manager(flask_app)
manager.add_command('db', MigrateCommand)

db.create_all()

# initialize the log handler
logHandler = RotatingFileHandler('info.log', maxBytes=1000, backupCount=1)

# set the log handler level
logHandler.setLevel(logging.INFO)

# set the app logger level
flask_app.logger.setLevel(logging.INFO)

flask_app.logger.addHandler(logHandler)
