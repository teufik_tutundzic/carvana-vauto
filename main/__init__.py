from celery import Celery
from .config import config_by_name

config = config_by_name['dev']
celery = Celery(__name__,
                broker=config.CELERY_BROKER_URL,
                include=['main.tasks'])
