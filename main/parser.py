from robobrowser import RoboBrowser
import re
import json
from urllib.parse import unquote

from main.db_handler import define_result_instances
from main.db_handler import save_result_instances


def add_vauto_urls(vins_provider_ids):
    template = "Vin:%22{}%22,ProviderId:{}," \
               "OtherCost:1000,TransportationCost:200,CertificationCost:300,AuctionCost:200,OtherCost:599," \
               "ProfitObjective:2000"
    for vin_pid in vins_provider_ids:
        query_string = template.format(vin_pid[0], vin_pid[1])
        query_string = '{' + query_string + '}'
        url = "http://test2.vauto.com/Va/Appraisal/AppraisalEdit.aspx?InitialData={}&AppraisalValue=10000"
        vin_pid.append(url.format(query_string))


def parse_input_url(url):
    decoded_url = unquote(url)
    re_str = '{}:"?\w+"?'
    desired_fields = ['Vin', 'ProviderId']
    fields = [re.findall(re_str.format(field), decoded_url)[0] for field in desired_fields if field in decoded_url]

    collection = dict()
    [helper_parse(item, collection) for item in fields]
    return collection


def save_and_format_vin_data(vins_provider_ids):
    v_auto_data = list()
    result_instances = list()
    for vin_pid_url in vins_provider_ids:
        input_values = {'vin': vin_pid_url[0], 'provider_id': vin_pid_url[1], 'url': vin_pid_url[2]}

        result_instance = define_result_instances(input_values)
        if result_instance:
            v_auto_data.append(input_values)
            result_instances.append(result_instance)

    save_result_instances(result_instances) if result_instances else None
    return v_auto_data


def fetch_site(url, headers, data=None):
    browser = RoboBrowser(history=True)
    for header, value in headers.items():
        browser.session.headers[header] = value

    browser.open(url, method='post', data={'data': json.dumps(data)}) if data else browser.open(url)
    return browser.response.text


def helper_parse(item, collection):
    break_point = item.find(':')

    if break_point < 0:
        collection[item] = 0
        return collection

    value = eval(item[break_point + 1:]) if item[break_point + 1:] != 'null' else None
    field = item[:break_point]

    if re.findall('"', item[:break_point]):
        collection[eval(field)] = value
    else:
        collection[field] = value
    return collection


def construct_radar_data_request(response_html):
    re_str = '"{}":"?[\w\s\.\-]*"?'
    desired_fields = ['Odometer', 'ModelYear', 'Make', 'Model', 'Series', 'Wheelbase', 'BodyDescription', 'BodyType',
                      'BodyDoorCount', 'EngineDescription', 'EngineCylinderCount', 'EngineDisplacement',
                      'EngineFuelIntake', 'EngineFuelType', 'EngineCamshaft', 'TransmissionDescription',
                      'TransmissionType', 'TransmissionGearCount', 'DriveTrainDescription', 'DriveTrainType',
                      'DriveTrainWheelCount', 'ExteriorColor', 'InteriorColor', 'StockNumber', 'distance',
                      'autoDistance', 'marketMode', 'SeriesDetail']

    fields = [re.findall(re_str.format(field), response_html)[0] for field in desired_fields if field in response_html]

    form_data = dict()
    for field in fields:
        break_point = field.find(':')
        value = eval(field[break_point + 1:]) if field[break_point + 1:] != 'null' else None
        form_data[eval(field[:break_point])] = value
    form_data['g'] = 'Radar'
    form_data['a'] = []
    form_data['autoDistance'] = None
    form_data['r'] = True
    form_data['PostalCode'] = ""
    return form_data


def find_checked_options(response_html):
    f = re.findall('"f":\[{.*}\]', response_html)
    f = '{' + f[0] + '}'
    f_options = json.loads(f)
    options = f_options.get('f')
    return [suboption for option in options if option for suboption in option.get('o') if suboption.get('s')]


def format_checked_options(checked_options):
    return [{"value": checked_option['d'], "name": checked_option['f']} for checked_option in checked_options
            if not checked_option.get('ro')]


def construct_a_field(checked_options, form_data):
    after_check_field = [{"id": checked_option['id'], "fid": checked_option['f'], "d": True} for checked_option in
                         checked_options]
    after_check_field.append({"id": "0", "fid": "IsCertified", "d": False})
    form_data['a'] = after_check_field
    return form_data


def extract_desired_values(response_html):
    mold = '"{}":\d+'
    desired_fields = ['racvc', 'o', 'average', 'eAverage', 'averageo']

    desired_values = [re.findall(mold.format(field), response_html) or [field] for field in desired_fields]
    collection = dict()
    [helper_parse(item[0], collection) for item in desired_values]
    return collection


def add_basic_fields(processed_dict, form_data):
    basic_keys = ['ModelYear', 'Make', 'Model']
    basic_fields_dict = {key: form_data[key] for key in basic_keys}
    processed_dict.update(basic_fields_dict)


def construct_notification_message(vin_status_list):
    if isinstance(vin_status_list, list):
        successes = [element['vin'] for element in vin_status_list if element.get('desired_values')]
        fails = [element['vin'] for element in vin_status_list if
                 not element.get('desired_values') and not element.get('error')]
        errors = [element['vin'] for element in vin_status_list if element.get('error')]
        message = "Processing of {} VINs was a success.{}.\n".format(len(successes), str(successes))
        message += "Processing of {} VINs was a failure:{}\n".format(len(fails), str(fails))
        message += "Among the failed VINs {} resulted in an error:{}".format(len(errors), str(errors))
    else:
        if vin_status_list.get('desired_values'):
            message = "Processing of VIN: {} was a success.\n".format(vin_status_list['vin'])
        elif vin_status_list.get('error'):
            message = "Processing of VIN: {} resulted in an error.\n".format(vin_status_list['vin'])
        else:
            message = "Processing of VIN: {} was a failure.\n".format(vin_status_list['vin'])

    return message
