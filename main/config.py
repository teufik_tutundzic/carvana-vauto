import os

basedir = os.path.abspath(os.path.dirname(__file__))


class Config:
    SECRET_KEY = os.getenv('SECRET_KEY', 'my_precious_secret_key')
    DEBUG = False
    RADAR_URL = "http://test2.vauto.com/Va/Appraisal/Guide/RadarDataHandler.ashx"
    LOGIN_URL = "https://test2.vauto.com/Va/Share/Login.aspx"


class DevelopmentConfig(Config):
    DEBUG = True
    LOGIN_USERNAME = "carvanaautobid"
    LOGIN_PASSWORD = "Carvana1"
    SQLALCHEMY_DATABASE_URI = 'mysql://root:root@localhost/v_auto_carvana'
    CACHE_TIMEOUT = 12 * 60 * 60
    CHROME_DRIVER = '/Users/teufiktutundzic/Documents/chromedriver'
    CELERY_BROKER_URL = "amqp://teufik:carvana993@localhost:5672/myvhost"
    CELERY_BACKEND_URL = "redis://localhost:6379/0"


class ProductionConfig(Config):
    DEBUG = False
    CHROME_DRIVER = '/Users/teufiktutundzic/Documents/chromedriver'


config_by_name = dict(
    dev=DevelopmentConfig,
    prod=ProductionConfig
)

key = Config.SECRET_KEY
