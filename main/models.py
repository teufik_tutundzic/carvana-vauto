from sqlalchemy.sql import func

from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


class ResultModel(db.Model):
    __tablename__ = 'tbl_results'

    created = db.Column(db.DateTime(timezone=True), server_default=func.now())
    updated = db.Column(db.DateTime(timezone=True), onupdate=func.now())
    id = db.Column(db.Integer, primary_key=True)
    url = db.Column(db.String(length=400), unique=True, nullable=False)
    vin = db.Column(db.String(length=17), unique=True, nullable=False)
    year = db.Column(db.Integer())
    make = db.Column(db.String(length=50))
    model = db.Column(db.String(length=50))
    provider_id = db.Column(db.Integer(), nullable=False)
    num_vehicles = db.Column(db.Integer(), default=0)
    avg_market_price = db.Column(db.Float(), default=0)
    avg_odometer = db.Column(db.Float(), default=0)
    odometer_adj = db.Column(db.Float(), default=0)
    series = db.Column(db.String(length=50))
    series_detail = db.Column(db.String(length=50))
    num_cylinders = db.Column(db.String(length=50))
    drive_train_type = db.Column(db.String(length=20))
    transmission = db.Column(db.String(length=50))
    displacement = db.Column(db.String(length=50))
    cab_style = db.Column(db.String(length=50))

    def __init__(self, url, vin=None, year=None, make=None, model=None, provider_id=None, num_vehicles=None,
                 avg_market_price=None, avg_odometer=None, odometer_adj=None, series=None, series_detail=None,
                 num_cylinders=None, drive_train_type=None, transmission=None, displacement=None, cab_style=None):
        self.url = url
        self.vin = vin if vin else self.vin
        self.year = year
        self.make = make
        self.model = model
        self.provider_id = provider_id if provider_id else self.provider_id
        self.num_vehicles = num_vehicles
        self.avg_market_price = avg_market_price
        self.avg_odometer = avg_odometer
        self.odometer_adj = odometer_adj
        self.series = series
        self.series_detail = series_detail
        self.num_cylinders = num_cylinders
        self.drive_train_type = drive_train_type
        self.transmission = transmission
        self.displacement = displacement
        self.cab_style = cab_style

    def __repr__(self):
        return '<vin:{}/ provider:{}>'.format(self.vin, self.provider_id)
