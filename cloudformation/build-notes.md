# Infrastructure config for kbb_valuation service
This services uses the following cloudformation template:
 *  /libs/config_env/cloudformation_templates/carvana-web-api.json

The following parameters are using defaults as defined in template:
 *  WebGroupInstanceMaxCapacity
 *  WebGroupInstanceDesiredCapacity

Note: this configuration assumes that apache config files /etc/apache2/apache.conf and /etc/apache2/sites_enabled

For VSTS build see:
 *  https://carvanadev.visualstudio.com/Carvana.VAuto/Carvana.VAuto%20Team/_build/index?path=%5CCarvana.VAutoPipeline&_a=allDefinitions
