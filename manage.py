from main import celery
from main.app import flask_app
from main.make_celery import init_celery

init_celery(flask_app, celery)
# application.run()
